package com.amaysim.cart;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.amaysim.model.ItemOrder;
import com.amaysim.model.Product;
import com.amaysim.promotions.PromoCode;
import com.amaysim.promotions.SpecialOffer;

/**
 * @author joseph cruz
 * 
 * The Shopping Cart implementation that accepts order
 * and computes its total price including promos and
 * special offers.
 *
 */
public class ShoppingCart {

	public Set<ItemOrder> items;
	
	private List<SpecialOffer> pricingRules;
	
	private List<String> promoCodes;
	
	public BigDecimal total;
	
	public ShoppingCart(List<SpecialOffer> pricingRules) {
		this.total = BigDecimal.ZERO;
		this.pricingRules = pricingRules;
		this.items = new HashSet<ItemOrder>();
		this.promoCodes = new ArrayList<String>();
	}

	/**
	 * Overloaded add method that add Item orders 
	 * (products with quantity) in the cart
	 * 
	 * @param itemOrder
	 * @param couponCode
	 */
	public synchronized void add(ItemOrder itemOrder, String... couponCode) {
		updateItems(itemOrder);
		
        if (couponCode.length > 0) {
        	promoCodes.add(couponCode[0]);
        }
	}
	
	/**
	 * Overloaded add method that add product 
	 * rather than item order in the cart
	 * 
	 * @param itemOrder
	 * @param couponCode
	 */	
	public synchronized void add(Product product, String... couponCode) {
		final ItemOrder itemOrder = new ItemOrder(product, 1);
		
		updateItems(itemOrder);
        
        if (couponCode.length > 0) {
        	promoCodes.add(couponCode[0]);
        }
	}
	
	private void updateItems(ItemOrder itemOrder) {
		if (items.contains(itemOrder)) {
			Iterator<ItemOrder> itemOrderIter = items.iterator();
			
			while (itemOrderIter.hasNext()) {
				ItemOrder itemOrderElem = itemOrderIter.next();
				
				if (itemOrderElem.equals(itemOrder)) {
					itemOrderElem.incrementQuantityBy(itemOrder.getQuantity());
				}
			}
		} else {
			items.add(itemOrder);
		}
	}
	
    /**
     * Calculates the total cost of the order. This will consider 
     * all the special offers and promos when it calculates the
     * total cost.
     *
     */
    public synchronized void calculateTotal() {
    	BigDecimal currTotal = calculateTotalWithoutDiscounts();
    	
    	for (SpecialOffer pricingRule : pricingRules) {
    		if (pricingRule instanceof PromoCode) {
    			final PromoCode promo = ((PromoCode) pricingRule);
    			final String couponCode = promo.getCode();
    			
    			if (promoCodes.contains(couponCode)) {
    				BigDecimal promoPrice = pricingRule.applyPricingRule(items, currTotal);  
    				
    				// set current total to the promo price
    				currTotal = promoPrice;
    			}
    			
    			// if the pricing rule has a promo code and it is not entered by the
    			// client then do not run the discounts logic and proceed 
    			// to the next pricing rule in the list
    			continue;  
    		}
    		
    		// if you get here this means that pricing rule does not have a promo code
    		BigDecimal discountedPrice = pricingRule.applyPricingRule(items, currTotal);
    		
    		// set current total to the discounted price
    		currTotal = discountedPrice;
    	}
    	
    	// set computed total
    	total = currTotal;
    }

    /**
     * Calculates and returns the total cost of the order 
     * without the promos and special offers
     *
     */
    public synchronized BigDecimal calculateTotalWithoutDiscounts() {
    	BigDecimal amount = BigDecimal.ZERO;
    	
    	// add total actual price per item type in the cart
    	for (ItemOrder itemOrder : items) {
    		BigDecimal newAmount = amount.add(itemOrder.getActualTotalPrice());
    		
    		amount = newAmount;
    	}
    	
    	return amount;
    }

    /**
     * Returns the total price of the cart
     *
     */
	public synchronized BigDecimal getTotal() {
		calculateTotal();
		
		return total.setScale(2, RoundingMode.CEILING);
	}

    /**
     * Returns the list of shopping cart items
     *
     */
    public synchronized Set<ItemOrder> getItems() {
        return items;
    }
    
    /**
     * Empties the shopping cart. Only the items and the
     * promo codes entered by the client are removed not 
     * the different pricing rules 
     * (special offers or promos).
     *
     */
    public synchronized void clear() {
        items.clear();
        promoCodes.clear();
        total = BigDecimal.ZERO;
    }
    
}
