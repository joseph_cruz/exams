package com.amaysim.promotions;

/**
 * @author joseph cruz
 * 
 * This interface is being implemented by different 
 * classes implement logic for each promo code.
 *
 */
public interface PromoCode {
	
	// promocode for this promo
	String getCode();
	
}
