package com.amaysim.promotions;

import java.math.BigDecimal;
import java.util.Set;

import com.amaysim.model.ItemOrder;


/**
 * @author joseph cruz
 *
 * Interface for the different pricing rules or special offers. Implementation
 * of this interface will implement the special promo logic.
 * 
 * This is using the Strategy Design Pattern and Programming 
 * for Interface not implementation principle.
 * 
 */
public interface SpecialOffer {
	
	BigDecimal applyPricingRule(Set<ItemOrder> itemOrders, BigDecimal currentPrice);
	
}
