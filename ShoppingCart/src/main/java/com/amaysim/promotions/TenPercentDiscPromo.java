package com.amaysim.promotions;

import java.math.BigDecimal;
import java.util.Set;

import com.amaysim.constants.ShoppinCartContstant;
import com.amaysim.model.ItemOrder;

/**
 * @author joseph cruz
 * 
 * Ten Percent discount accross the board for 'I<3AMAYSIM'
 *
 */
public class TenPercentDiscPromo implements SpecialOffer, PromoCode {
	
	@Override
	public String getCode() {
		return ShoppinCartContstant.TEN_PERCENT_ACROSS_THE_BOARD_CODE;
	}

	@Override
	public BigDecimal applyPricingRule(Set<ItemOrder> itemOrders, BigDecimal currentPrice) {
		// apply 10% discount
		return currentPrice.multiply(new BigDecimal(0.9));
	}

}
