package com.amaysim.promotions;

import java.math.BigDecimal;
import java.util.Set;

import com.amaysim.constants.ShoppinCartContstant;
import com.amaysim.model.ItemOrder;


/**
 * @author joseph cruz
 * 
 * A 3 for 2 deal on Unlimited 1GB Sims. So for example, if you buy 3 Unlimited 1GB Sims, 
 * you will pay the price of 2 only for the first month.
 *
 */
public class ThreeForTwoUltSmall implements SpecialOffer {

	@Override
	public BigDecimal applyPricingRule(final Set<ItemOrder> itemOrders, BigDecimal currentPrice) {
		BigDecimal deductiblePrice = BigDecimal.ZERO;
		BigDecimal newPrice = currentPrice;
		
		for (ItemOrder itemOrder : itemOrders) {
			if (itemOrder.getProductCode().equals(ShoppinCartContstant.UNLIMITED_SMALL)) {
				final int noOfItems = itemOrder.getQuantity();
				
				// get items by three to know how many items' price will be deducted
				final int noDivisibleByThree = noOfItems / 3;
				final BigDecimal noDivisibleByThreeBigDec = new BigDecimal(noDivisibleByThree);
				
				// compute deductible price and substract it from the current price
				deductiblePrice = noDivisibleByThreeBigDec.multiply(itemOrder.getProductPrice());
				newPrice = newPrice.subtract(deductiblePrice);
				
				// store the discount price for the 1st month
				itemOrder.getDiscountedPricePerMonth().put(1, newPrice);
				
				// no need to process other type of items
				break;
			}
		}
		
		// return the computed price in case there are discounts
		return newPrice;
	}

}
