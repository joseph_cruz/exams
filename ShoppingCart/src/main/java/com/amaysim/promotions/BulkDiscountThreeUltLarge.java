package com.amaysim.promotions;

import java.math.BigDecimal;
import java.util.Set;

import com.amaysim.constants.ShoppinCartContstant;
import com.amaysim.model.ItemOrder;

/**
 * @author joseph cruz
 *
 * The Unlimited 5GB Sim will have a bulk discount applied; 
 * whereby the price will drop to $39.90 each for the 
 * first month, if the customer buys more than 3.
 *
 */
public class BulkDiscountThreeUltLarge implements SpecialOffer {

	@Override
	public BigDecimal applyPricingRule(final Set<ItemOrder> itemOrders, final BigDecimal currentPrice) {
		BigDecimal deductiblePrice = BigDecimal.ZERO;
		BigDecimal newPrice = currentPrice;
		
		for (ItemOrder itemOrder : itemOrders) {
			final int noOfItems = itemOrder.getQuantity();
			final BigDecimal noOfItemsBigDec = new BigDecimal(noOfItems);
			
			// check if there ult_large items and if they are more than 3
			if (itemOrder.getProductCode().equals(ShoppinCartContstant.UNLIMITED_LARGE) && noOfItems > 3) {
				final BigDecimal currentProductPrice = itemOrder.getProductPrice();
				
				// get deductible price from original price
				BigDecimal lessPrice = currentProductPrice.subtract(new BigDecimal(39.90));
				
				// compute the new discounted price
				deductiblePrice = noOfItemsBigDec.multiply(lessPrice);
				newPrice = newPrice.subtract(deductiblePrice);
				
				// store the discount price for the 1st month
				itemOrder.getDiscountedPricePerMonth().put(1, newPrice);
				
				// no need to process other type of items
				break;
			}
		}

		// return the computed price in case there are discounts
		return newPrice;
	}

}
