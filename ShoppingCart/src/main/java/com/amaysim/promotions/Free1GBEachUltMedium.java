package com.amaysim.promotions;

import java.math.BigDecimal;
import java.util.Set;

import com.amaysim.constants.ShoppinCartContstant;
import com.amaysim.model.Catalogue;
import com.amaysim.model.ItemOrder;
import com.amaysim.model.Product;


/**
 * @author joseph cruz
 * 
 * We will bundle in a free 1 GB Data-pack free-of-charge with every Unlimited 2GB sold.
 *
 */
public class Free1GBEachUltMedium implements SpecialOffer {

	@Override
	public BigDecimal applyPricingRule(final Set<ItemOrder> itemOrders, final BigDecimal currentPrice) {
		int noOfUltMedium = 0;
		
		// loop through the itemOrders first to check for the number of ult_medium products
		for (ItemOrder itemOrder : itemOrders) {
			if (itemOrder.getProductCode().equals(ShoppinCartContstant.UNLIMITED_MEDIUM)) {
				// get number of ult_medium products
				noOfUltMedium = itemOrder.getQuantity();
				
				// no need to process other type of items
				break;
			}
		}
		
		// add 1GB data pack for each ult_medium
		if (noOfUltMedium != 0) {		
			Catalogue catalogue = Catalogue.newInstance();
			Set<Product> products = catalogue.getProducts();
			
			for (Product product : products) {
				if (product.getCode().equals(ShoppinCartContstant.ONE_GB_DATA_PACK)) {
					ItemOrder itemOrderMedium = new ItemOrder(product, noOfUltMedium);
					
					itemOrders.add(itemOrderMedium);
				}
			}			
		}
		
		return currentPrice;
	}

}
