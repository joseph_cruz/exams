package com.amaysim.main;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.amaysim.cart.ShoppingCart;
import com.amaysim.constants.ShoppinCartContstant;
import com.amaysim.model.Catalogue;
import com.amaysim.model.ItemOrder;
import com.amaysim.model.Product;
import com.amaysim.promotions.BulkDiscountThreeUltLarge;
import com.amaysim.promotions.Free1GBEachUltMedium;
import com.amaysim.promotions.SpecialOffer;
import com.amaysim.promotions.TenPercentDiscPromo;
import com.amaysim.promotions.ThreeForTwoUltSmall;

/**
 * @author joseph cruz
 * 
 * Test class
 *
 */
public class Runner {
	
	public static void main(String[] args) {
		/**
		 * Setup product catalog. This should be created once in an application and passed to
		 * shopping cart so that the shopping cart will know which catalog to use. This is 
		 * better 
		 */
		final Product productSmall = new Product("ult_small", "Unlimited 1GB", new BigDecimal(24.90));
		final Product productMedium = new Product("ult_medium", "Unlimited 2GB", new BigDecimal(29.90));
		final Product productLarge = new Product("ult_large", "Unlimited 5GB", new BigDecimal(44.90));
		final Product productOneGBDataPack = new Product("1gb", "1 GB Data-pack", new BigDecimal(9.90));
		
		final Catalogue catalogue = Catalogue.newInstance();
		catalogue.addProduct(productSmall);
		catalogue.addProduct(productMedium);
		catalogue.addProduct(productLarge);
		catalogue.addProduct(productOneGBDataPack);
		
		/**
		 * Set pricing rules
		 */
		final List<SpecialOffer> pricingRules = new ArrayList<SpecialOffer>();
		pricingRules.add(new ThreeForTwoUltSmall());
		pricingRules.add(new BulkDiscountThreeUltLarge());
		pricingRules.add(new Free1GBEachUltMedium());
		pricingRules.add(new TenPercentDiscPromo());
		
		// Create shopping cart
		ShoppingCart shoppingCart = new ShoppingCart(pricingRules);
		
		/**
		 * Scenario 1
		 */
		System.out.println("Scenario 1: Fresh Cart");
		
		System.out.println("Items Added: ");
		System.out.println("3 x " + productSmall.getName());
		System.out.println("1 x " + productLarge.getName());
		shoppingCart.add(new ItemOrder(productSmall, 3));
		shoppingCart.add(new ItemOrder(productLarge, 1));
			
		System.out.println("\nExpected Cart Total " + shoppingCart.getTotal());
		System.out.println("\nExpected Cart Items: ");	
		for (ItemOrder item : shoppingCart.getItems()) {
			System.out.println(item.getQuantity() + " x " + item.getProductName());
		}
		
		System.out.println("\n");
		
		/**
		 * Scenario 2
		 */
		shoppingCart.clear();
		
		System.out.println("Scenario 2: Fresh Cart");
		
		System.out.println("Items Added: ");
		System.out.println("2 x " + productSmall.getName());
		System.out.println("4 x " + productLarge.getName());
		shoppingCart.add(new ItemOrder(productSmall, 2));
		shoppingCart.add(new ItemOrder(productLarge, 4));
		
		System.out.println("\nExpected Cart Total " + shoppingCart.getTotal());
		System.out.println("\nExpected Cart Items: ");
		for (ItemOrder item : shoppingCart.getItems()) {
			System.out.println(item.getQuantity() + " x " + item.getProductName());
		}
		
		System.out.println("\n");
				
		/**
		 * Scenario 3
		 */
		shoppingCart.clear();
		
		System.out.println("Scenario 3: Fresh Cart");
		
		System.out.println("Items Added: ");
		System.out.println("1 x " + productSmall.getName());
		System.out.println("2 x " + productMedium.getName());		
		shoppingCart.add(new ItemOrder(productSmall, 1));
		shoppingCart.add(new ItemOrder(productMedium, 2));
		
		System.out.println("\nExpected Cart Total " + shoppingCart.getTotal());
		System.out.println("\nExpected Cart Items: ");
		
		for (ItemOrder item : shoppingCart.getItems()) {
			System.out.println(item.getQuantity() + " x " + item.getProductName());
		}
		
		System.out.println("\n");
		
		/**
		 * Scenario 4
		 */
		shoppingCart.clear();
		
		System.out.println("Scenario 4: Fresh Cart");
		
		System.out.println("Items Added: ");
		System.out.println("1 x " + productSmall.getName());
		System.out.println("1 x " + productOneGBDataPack.getName());
		System.out.println(ShoppinCartContstant.TEN_PERCENT_ACROSS_THE_BOARD_CODE + " Promo Applied");
		shoppingCart.add(new ItemOrder(productSmall, 1));
		shoppingCart.add(new ItemOrder(productOneGBDataPack, 1), ShoppinCartContstant.TEN_PERCENT_ACROSS_THE_BOARD_CODE);
		
		System.out.println("\nExpected Cart Total " + shoppingCart.getTotal());
		System.out.println("\nExpected Cart Items: ");
		
		for (ItemOrder item : shoppingCart.getItems()) {
			System.out.println(item.getQuantity() + " x " + item.getProductName());
		}
		
		System.out.println("\n");
		
		/**
		 * Scenario 5
		 */
		shoppingCart.clear();
		
		System.out.println("Scenario 5: Fresh Cart");
		System.out.println("This is a scenario to test adding product rather than item order.");
		
		System.out.println("Items Added: ");
		System.out.println("3 x " + productSmall.getName());
		shoppingCart.add(new ItemOrder(productSmall, 1));
		shoppingCart.add(new ItemOrder(productSmall, 1));
		shoppingCart.add(new ItemOrder(productSmall, 1));
		
		System.out.println("\nExpected Cart Total " + shoppingCart.getTotal());
		System.out.println("\nExpected Cart Items: ");
		
		for (ItemOrder item : shoppingCart.getItems()) {
			System.out.println(item.getQuantity() + " x " + item.getProductName());
		}
	}
	
}
