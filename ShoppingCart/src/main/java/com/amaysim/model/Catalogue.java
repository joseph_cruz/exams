package com.amaysim.model;

import java.util.HashSet;
import java.util.Set;


/**
 * @author joseph cruz
 * 
 * This model represents the single catalogue in our Shopping Cart application.
 * The catalogue consists of multiple products or items that we can add to
 * our shopping cart. I assumed there is only one catalogue for the whole 
 * application. So this single catalogue will be represented by a single
 * Catalogue object. Thus, this will use the Singleton Pattern.
 *
 */
public class Catalogue {
	
	private static Catalogue catalogue = new Catalogue();
	
	private Set<Product> products = new HashSet<Product>();

	private Catalogue() {
	}
	
	public static Catalogue newInstance() {
		return catalogue;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void addProduct(Product product) {
		products.add(product);
	}

	@Override
	public String toString() {
		return new StringBuilder("Catalog [products=")
			.append(products).append("]").toString();
	}

}
