package com.amaysim.model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * @author joseph cruz
 *
 * ShoppingCartItem represents the specific item type
 * that is already in the cart. It also consists
 * of the number of items of that type in the cart
 * 
 */
public class ItemOrder {
	
	private Product product;
	
	private int quantity;
	
	private Map<Integer, BigDecimal> discountedPricePerMonth = new HashMap<Integer, BigDecimal>();

	public ItemOrder(Product product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}
	
	public BigDecimal getActualTotalPrice() {
		final BigDecimal productPrice = getProductPrice();
		final BigDecimal newproductPrice = productPrice.multiply(new BigDecimal(quantity));
		
		return newproductPrice;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
    public void incrementQuantityBy(int quantityToAdd) {
        quantity += quantityToAdd;
    }

	public Map<Integer, BigDecimal> getDiscountedPricePerMonth() {
		return discountedPricePerMonth;
	}

	public void setDiscountedPricePerMonth(
			Map<Integer, BigDecimal> discountedPricePerMonth) {
		this.discountedPricePerMonth = discountedPricePerMonth;
	}
	
	public String getProductName() {
		return product.getName();
	}
	
	public String getProductCode() {
		return product.getCode();
	}
	
	public BigDecimal getProductPrice() {
		return product.getPrice();
	}

	public int hashCode()  {
		return 1;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ItemOrder) {
			final ItemOrder otherItemOrder = (ItemOrder) obj;
			
			final String thisProductCode = product.getCode();
			final String otherProductCode = otherItemOrder.getProductCode();
			
			return thisProductCode.equals(otherProductCode);
		}
		
		return false;
	}

	@Override
	public String toString() {
		return new StringBuilder("ItemOrder [product=").append(product)
				.append(", quantity=").append(quantity)
				.append("]").toString();
	}

}
