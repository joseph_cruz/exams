package com.amaysim.model;

import java.math.BigDecimal;

/**
 * @author joseph cruz
 * 
 * Product represents the specific item being added or 
 * removed from the shopping cart
 *
 */
public class Product {

	private String code;
	
	private String name;
	
	private BigDecimal price;

	public Product() {
	}

	public Product(String code, String name, BigDecimal price) {
		this.code = code;
		this.name = name;
		this.price = price;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int hashCode()  {
		return 1;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Product) {
			final Product otherProduct = (Product) obj;
			
			return code.equals(otherProduct.getCode());
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		return new StringBuilder("Product [code=").append(code)
				.append(", name=").append(name).append(", price=")
				.append(price).append("]").toString();
	}
	
}
