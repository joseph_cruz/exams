package com.amaysim.constants;


/**
 * @author joseph cruz
 *
 * This class will consists of all the constants that are 
 * being used by the Shopping Cart application.
 * 
 */
public class ShoppinCartContstant {
	
	// catalogue product codes
	public static final String UNLIMITED_SMALL = "ult_small";
	public static final String UNLIMITED_MEDIUM = "ult_medium";
	public static final String UNLIMITED_LARGE = "ult_large";
	public static final String ONE_GB_DATA_PACK = "1gb";
	
	// promo codes
	public static final String TEN_PERCENT_ACROSS_THE_BOARD_CODE = "I<3AMAYSIM";
	
}
