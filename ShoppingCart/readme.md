Shopping Cart
==================

##About the project

The ShoppingCart project is just a basic Java Project. It only includes the JDK 1.7 jars and does not use external libraries and frameworks.


##Test Class

The test class or client is com.amaysim.main.Runner. Runner consists of a main() method that can be run by just simply using the ���java��� command or run from eclipse. I included eclipse specific files to be imported to Eclipse IDE.


##Assumptions

I assumed that there is only one catalogue. Thus, I used singleton pattern to represent that catalogue of products.


##Design Pattern Used
   - Strategy
   - Decorator
   - Iterator
   - (Thread-Safe) Singleton Pattern


##Design Principle Used
   - Single Responsibility Principle
   - Open Closed Principle
   - Liskov Substitution Principle
   - Interface Segragation Principle
   - Dependency inversion principle
   - Hollywood Principle
   - Thin client fat model
